//
//  DropBox.h
//  DragAndDropTest
//
//  Created by John Watson on 5/1/13.
//

#import <UIKit/UIKit.h>
#import "JWDroppableTarget.h"

//*** JW STEP #5 ***//
@interface DropBox : UIView <JWDroppableTarget>

@property (strong, nonatomic) UILabel *dropHereLabel;

@property int dropCount;
@property UILabel *numlbl;

@end
