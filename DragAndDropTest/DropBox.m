//
//  DropBox.m
//  DragAndDropTest
//
//  Created by John Watson on 5/1/13.
//

#import "DropBox.h"
#import "JWDraggableTableViewCell.h"

@implementation DropBox

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initLabel];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Initialization code
        [self initLabel];
    }
    return self;    
}

-(void) initLabel
{
    //Add count labels in code...
    UILabel *countlbl = [[UILabel alloc] initWithFrame:CGRectMake(3, 150, 60, 25)];
    countlbl.backgroundColor = [UIColor clearColor];
    countlbl.text = @"Count: ";
    [self addSubview:countlbl];
    
    self.numlbl = [[UILabel alloc] initWithFrame:CGRectMake(61, 150, 60, 25)];
    self.numlbl.backgroundColor = [UIColor clearColor];
    self.numlbl.text = [NSString stringWithFormat:@"%i", 0];
    [self addSubview:self.numlbl];
}

-(void)droppableTargetHandleDragInWithPayload:(id)payload
{
    self.backgroundColor = [UIColor greenColor];
}

-(void) droppableTargetHandleDragOutWithPayload:(id)payload
{
    self.backgroundColor = [UIColor yellowColor];
}

-(void)droppableTargetHandleDragStartWithPayload:(id)payload
{
    if( !self.dropHereLabel )
    {
        int x = 10;
        int y = (self.frame.size.height/2) - 10;
        int height = 20;
        int width = self.frame.size.width - 20;
        
        self.dropHereLabel = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, height)];
        self.dropHereLabel.backgroundColor = [UIColor clearColor];
        self.dropHereLabel.font = [UIFont fontWithName:@"Helvetica" size:15.0];
        self.dropHereLabel.text = @"Drag Here";
    }
    
    [self addSubview:self.dropHereLabel];
}

-(void)droppableTargetHandleDragEndWithPayload:(id)payload
{
    [self.dropHereLabel removeFromSuperview];
    self.backgroundColor = [UIColor yellowColor];
}

-(BOOL)droppableTargetHandleDropSuccessWithPayload:(id)payload
{
    if( [payload isKindOfClass:[UITableViewCell class]] )
    {
        self.dropCount++;
        self.numlbl.text = [NSString stringWithFormat:@"%i", self.dropCount];
        return YES;
    }
    else
    {
        return NO;
    }
}

@end
