//
//  JWDraggingObject.h
//
//  Created by John Watson
//

#import <Foundation/Foundation.h>

// *** DESCRIPTION  *** 
// this class represents the "pseudo object" that is actually being dragged around the screen. It is different than the JWDraggableTableViewCell
// and only exists for the lifetime of the drag operation. It is primarily used to keep track of multiple objects simultaneously being dragged
// around the screen
// *** /DESCRIPTION ***

@interface JWDraggingObject : NSObject

@property (strong, nonatomic) UIView *originalView; //this should NOT be used for intersection code because it never actually "moves" - it is only used for sanity checks and for original position/payload information. For intersection and animations use the draggable image!
@property CGRect currentlyDraggingViewStartFrame;
@property (strong, nonatomic) UIImageView *draggableImage;

@end
