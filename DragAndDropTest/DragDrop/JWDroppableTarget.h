//
//  JWDroppableTarget.h
//
//  Created by John Watson
//

#import <Foundation/Foundation.h>

@class JWDraggableTableViewCell;

@protocol JWDroppableTarget <NSObject>

@optional
//NOTE: All of the protocol methods receive the payload of the dragging item when they are called.
//      You can then call isKindOfClass on the payload object to determine if it is a payload that your drop target should respond to,
//      or if your event method should return without taking action.

//these methods allow the implementing protocol to provide visual feedback when an item begins or ends the drag lifecycle
- (void)droppableTargetHandleDragStartWithPayload:(id)payload; //Called when dragging begins
- (void)droppableTargetHandleDragEndWithPayload:(id)payload; //Called when dragging ends and the dragging object was NOT over this drop target

//these methods allow the implementing protocol to respond to events that occur DURING the drag lifecycle
- (void)droppableTargetHandleDragInWithPayload:(id)payload; //Called when the dragging object enters this drop target
- (void)droppableTargetHandleDragOutWithPayload:(id)payload; //Called when the dragging object leaves this drop target
- (BOOL)droppableTargetHandleDropSuccessWithPayload:(id)payload; //Called when dragging ends and the dragging object is over this drop target. This method should return true if the target accepted the drop, false if it rejects the drop

@end
