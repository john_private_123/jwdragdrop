//
//  JWDragDropCoordinator.h
//
//  Created by John Watson
//

#import <Foundation/Foundation.h>
#import "JWDroppableTarget.h"

typedef enum _kJWDragDropZPosition {
    JW_DRAG_DROP_Z_POSITION_NONE,
    JW_DRAG_DROP_Z_POSITION_LOWEST,
    JW_DRAG_DROP_Z_POSITION_LOW,
    JW_DRAG_DROP_Z_POSITION_MEDIUM,
    JW_DRAG_DROP_Z_POSITION_HIGH,
    JW_DRAG_DROP_Z_POSITION_HIGHEST,
    } kJWDragDropZPosition;

@interface JWDragDropCoordinator : NSObject

@property (strong, nonatomic) UIView *globalCoordinateView;
@property (strong, nonatomic) NSArray *droppableTargets;

@property (strong, nonatomic) id<JWDroppableTarget> currentlyOverDropTarget; //the drop target that the user is currently dragging over

@property (strong, nonatomic) NSMutableDictionary *draggingObjects;


+(JWDragDropCoordinator *) sharedInstance;

// DROP TARGET METHODS
- (void) addDropTarget:(id)dropTarget withZPosition:(kJWDragDropZPosition)zPosition;
- (void) removeDropTarget:(id)dropTarget;

// DRAGGABLE ITEM METHODS
- (void) dragStartedForView:(UIView *)draggingView withTouch:(UITouch *)touch andPayload:(id)payload;
- (void) dragEndedForView:(UIView *)draggingView withTouch:(UITouch *)touch andPayload:(id)payload;
- (void) dragMovedForView:(UIView *)draggingView withTouch:(UITouch *)touch andPayload:(id)payload;

@end
