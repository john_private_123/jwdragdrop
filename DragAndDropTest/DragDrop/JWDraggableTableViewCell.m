//
//  JWDraggableView.m
//
//  Created by John Watson
//

#import "JWDraggableTableViewCell.h"
#import "JWDragDropCoordinator.h"

#define DROPPABLEVIEW_BEGIN_DRAG_DELAY_DEFAULT 0.1 //default drag delay

@interface JWDraggableTableViewCell()

@property (nonatomic, strong) NSTimer *dragStartTimer;
@property (nonatomic, strong) NSSet *dragStartTouches;

@end

@implementation JWDraggableTableViewCell

-(id)init
{
    self = [super init];
    if( self )
    {
        self.dragStartDelay = DROPPABLEVIEW_BEGIN_DRAG_DELAY_DEFAULT;
        self.dragEnabled = YES;
    }
    
    return self;
}

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if( self )
    {
        self.dragStartDelay = DROPPABLEVIEW_BEGIN_DRAG_DELAY_DEFAULT;
        self.dragEnabled = YES;
    }
    
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if( self )
    {
        self.dragStartDelay = DROPPABLEVIEW_BEGIN_DRAG_DELAY_DEFAULT;
        self.dragEnabled = YES;
    }
    
    return self;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if( self )
    {
        self.dragStartDelay = DROPPABLEVIEW_BEGIN_DRAG_DELAY_DEFAULT;
        self.dragEnabled = YES;
    }
    
    return self;
}



#pragma mark UIResponder (touch handling)
- (void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
{
    [super touchesBegan:touches withEvent:event];
    
    if( !self.dragEnabled )
        return;
    
    self.dragStartTouches = touches;
    self.dragStartTimer = [NSTimer scheduledTimerWithTimeInterval:self.dragStartDelay target:self selector:@selector(beginDrag:) userInfo:nil repeats:NO];
}

- (void)beginDrag:(NSTimer *)timer
{
    if( !self.dragEnabled )
        return;
    
    [self resetDragStartTimer];
    
    if( [self.superview isKindOfClass:[UIScrollView class]] )
        [((UIScrollView *)self.superview) setScrollEnabled:NO];
    
    [[JWDragDropCoordinator sharedInstance] dragStartedForView:self withTouch:[self.dragStartTouches anyObject] andPayload:self.payload];
}

- (void)touchesMoved:(NSSet*)touches withEvent:(UIEvent*)event
{
    [super touchesMoved:touches withEvent:event];
    
    if( !self.dragEnabled )
        return;
    
    [[JWDragDropCoordinator sharedInstance] dragMovedForView:self withTouch:[touches anyObject] andPayload:self.payload];
}

- (void)touchesEnded:(NSSet*)touches withEvent:(UIEvent*)event
{
    [super touchesEnded:touches withEvent:event];
    
    if( !self.dragEnabled )
        return;
    
    [self resetDragStartTimer];
    
    if( [self.superview isKindOfClass:[UIScrollView class]] )
        [((UIScrollView *)self.superview) setScrollEnabled:YES];
    
    [[JWDragDropCoordinator sharedInstance] dragEndedForView:self withTouch:[touches anyObject] andPayload:self.payload];
}

- (void)touchesCancelled:(NSSet*)touches withEvent:(UIEvent*)event
{
    [super touchesCancelled:touches withEvent:event];
    
    if( !self.dragEnabled )
        return;
    
    if( self.dragStartTimer )
    {
        [self.dragStartTimer invalidate];
        self.dragStartTimer = nil;
    }
    
    if( [self.superview isKindOfClass:[UIScrollView class]] )
    {
        UIScrollView *parentScrollView = (UIScrollView *)self.superview;
        [parentScrollView setScrollEnabled:YES];
    }
    
    [[JWDragDropCoordinator sharedInstance] dragEndedForView:self withTouch:[touches anyObject] andPayload:self.payload];
}

- (void) resetDragStartTimer
{
    if( self.dragStartTimer )
    {
        [self.dragStartTimer invalidate];
        self.dragStartTimer = nil;
    }
}

@end
