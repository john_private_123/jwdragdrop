//
//  JWDragDropNotificationConstants.m
//
//  Created by John Watson
//

#import "JWDragDropNotificationConstants.h"

NSString * const JWDragDropNotificationDisableTouchResponders = @"JWDragDropNotificaitonDisableTouchResponders";
NSString * const JWDragDropNotificationEnableTouchResponders  = @"JWDragDropNotificationEnableTouchResponders";