//
//  ViewController.h
//  DragAndDropTest
//
//  Created by John Watson
//

#import <UIKit/UIKit.h>

@class DropBox;

@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSMutableArray *dataSource;

@property (weak, nonatomic) IBOutlet DropBox *dropTarget1View;
@property (weak, nonatomic) IBOutlet DropBox *dropTarget2View;

@property (weak, nonatomic) IBOutlet UILabel *dropTarget1CountLabel;
@property (weak, nonatomic) IBOutlet UILabel *dropTarget2CountLabel;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
